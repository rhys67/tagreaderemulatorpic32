#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-PIC32MX250F128B.mk)" "nbproject/Makefile-local-PIC32MX250F128B.mk"
include nbproject/Makefile-local-PIC32MX250F128B.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=PIC32MX250F128B
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/vTagReader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/vTagReader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/cardReaderEmulator/src/3party/iso14443a_crc.c ../src/cardReaderEmulator/src/reader/reader_pns512.c ../src/cardReaderEmulator/src/reader/reader_uart_cmds.c ../src/cardReaderEmulator/src/reader/registers.c ../src/cardReaderEmulator/src/tag/tag_ntag213.c ../src/cardReaderEmulator/src/tag/tag_uart_cmds.c ../src/cardReaderEmulator/src/reader_tag_io_buffer.c ../src/cardReaderEmulator/src/uart_cmds.c ../src/pageSamples/nature_myTag.c ../src/pageSamples/test2.c ../src/system_config/PIC32MX250F128B/framework/system/clk/src/sys_clk_static.c ../src/system_config/PIC32MX250F128B/framework/system/ports/src/sys_ports_static.c ../src/system_config/PIC32MX250F128B/system_init.c ../src/system_config/PIC32MX250F128B/system_interrupt.c ../src/system_config/PIC32MX250F128B/system_exceptions.c ../src/system_config/PIC32MX250F128B/system_tasks.c ../src/usb/UART_CDC_Link.c ../src/usb/USBCDC_wrapper.c ../src/app.c ../src/main.c ../src/tag_eeprom.c ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs.c ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs_device.c ../../../../framework/system/devcon/src/sys_devcon.c ../../../../framework/system/devcon/src/sys_devcon_pic32mx.c ../../../../framework/system/int/src/sys_int_pic32.c ../../../../framework/system/reset/src/sys_reset.c ../../../../framework/usb/src/dynamic/usb_device.c ../../../../framework/usb/src/dynamic/usb_device_cdc.c ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o ${OBJECTDIR}/_ext/649610267/reader_pns512.o ${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o ${OBJECTDIR}/_ext/649610267/registers.o ${OBJECTDIR}/_ext/986819774/tag_ntag213.o ${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o ${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o ${OBJECTDIR}/_ext/1214191177/uart_cmds.o ${OBJECTDIR}/_ext/54334336/nature_myTag.o ${OBJECTDIR}/_ext/54334336/test2.o ${OBJECTDIR}/_ext/384435606/sys_clk_static.o ${OBJECTDIR}/_ext/1417152314/sys_ports_static.o ${OBJECTDIR}/_ext/324476799/system_init.o ${OBJECTDIR}/_ext/324476799/system_interrupt.o ${OBJECTDIR}/_ext/324476799/system_exceptions.o ${OBJECTDIR}/_ext/324476799/system_tasks.o ${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o ${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o ${OBJECTDIR}/_ext/1360937237/app.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/tag_eeprom.o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ${OBJECTDIR}/_ext/764219029/drv_usbfs.o ${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o ${OBJECTDIR}/_ext/1271179505/sys_devcon.o ${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ${OBJECTDIR}/_ext/813494389/sys_reset.o ${OBJECTDIR}/_ext/610166344/usb_device.o ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o.d ${OBJECTDIR}/_ext/649610267/reader_pns512.o.d ${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o.d ${OBJECTDIR}/_ext/649610267/registers.o.d ${OBJECTDIR}/_ext/986819774/tag_ntag213.o.d ${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o.d ${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o.d ${OBJECTDIR}/_ext/1214191177/uart_cmds.o.d ${OBJECTDIR}/_ext/54334336/nature_myTag.o.d ${OBJECTDIR}/_ext/54334336/test2.o.d ${OBJECTDIR}/_ext/384435606/sys_clk_static.o.d ${OBJECTDIR}/_ext/1417152314/sys_ports_static.o.d ${OBJECTDIR}/_ext/324476799/system_init.o.d ${OBJECTDIR}/_ext/324476799/system_interrupt.o.d ${OBJECTDIR}/_ext/324476799/system_exceptions.o.d ${OBJECTDIR}/_ext/324476799/system_tasks.o.d ${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o.d ${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o.d ${OBJECTDIR}/_ext/1360937237/app.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/1360937237/tag_eeprom.o.d ${OBJECTDIR}/_ext/280795049/drv_i2c.o.d ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d ${OBJECTDIR}/_ext/764219029/drv_usbfs.o.d ${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o.d ${OBJECTDIR}/_ext/1271179505/sys_devcon.o.d ${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o.d ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d ${OBJECTDIR}/_ext/813494389/sys_reset.o.d ${OBJECTDIR}/_ext/610166344/usb_device.o.d ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o ${OBJECTDIR}/_ext/649610267/reader_pns512.o ${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o ${OBJECTDIR}/_ext/649610267/registers.o ${OBJECTDIR}/_ext/986819774/tag_ntag213.o ${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o ${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o ${OBJECTDIR}/_ext/1214191177/uart_cmds.o ${OBJECTDIR}/_ext/54334336/nature_myTag.o ${OBJECTDIR}/_ext/54334336/test2.o ${OBJECTDIR}/_ext/384435606/sys_clk_static.o ${OBJECTDIR}/_ext/1417152314/sys_ports_static.o ${OBJECTDIR}/_ext/324476799/system_init.o ${OBJECTDIR}/_ext/324476799/system_interrupt.o ${OBJECTDIR}/_ext/324476799/system_exceptions.o ${OBJECTDIR}/_ext/324476799/system_tasks.o ${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o ${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o ${OBJECTDIR}/_ext/1360937237/app.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/tag_eeprom.o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ${OBJECTDIR}/_ext/764219029/drv_usbfs.o ${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o ${OBJECTDIR}/_ext/1271179505/sys_devcon.o ${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ${OBJECTDIR}/_ext/813494389/sys_reset.o ${OBJECTDIR}/_ext/610166344/usb_device.o ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o

# Source Files
SOURCEFILES=../src/cardReaderEmulator/src/3party/iso14443a_crc.c ../src/cardReaderEmulator/src/reader/reader_pns512.c ../src/cardReaderEmulator/src/reader/reader_uart_cmds.c ../src/cardReaderEmulator/src/reader/registers.c ../src/cardReaderEmulator/src/tag/tag_ntag213.c ../src/cardReaderEmulator/src/tag/tag_uart_cmds.c ../src/cardReaderEmulator/src/reader_tag_io_buffer.c ../src/cardReaderEmulator/src/uart_cmds.c ../src/pageSamples/nature_myTag.c ../src/pageSamples/test2.c ../src/system_config/PIC32MX250F128B/framework/system/clk/src/sys_clk_static.c ../src/system_config/PIC32MX250F128B/framework/system/ports/src/sys_ports_static.c ../src/system_config/PIC32MX250F128B/system_init.c ../src/system_config/PIC32MX250F128B/system_interrupt.c ../src/system_config/PIC32MX250F128B/system_exceptions.c ../src/system_config/PIC32MX250F128B/system_tasks.c ../src/usb/UART_CDC_Link.c ../src/usb/USBCDC_wrapper.c ../src/app.c ../src/main.c ../src/tag_eeprom.c ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs.c ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs_device.c ../../../../framework/system/devcon/src/sys_devcon.c ../../../../framework/system/devcon/src/sys_devcon_pic32mx.c ../../../../framework/system/int/src/sys_int_pic32.c ../../../../framework/system/reset/src/sys_reset.c ../../../../framework/usb/src/dynamic/usb_device.c ../../../../framework/usb/src/dynamic/usb_device_cdc.c ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-PIC32MX250F128B.mk dist/${CND_CONF}/${IMAGE_TYPE}/vTagReader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX250F128B
MP_LINKER_FILE_OPTION=,--script="..\src\system_config\PIC32MX250F128B\app_mx.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o: ../src/cardReaderEmulator/src/3party/iso14443a_crc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1143853589" 
	@${RM} ${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o.d" -o ${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o ../src/cardReaderEmulator/src/3party/iso14443a_crc.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/649610267/reader_pns512.o: ../src/cardReaderEmulator/src/reader/reader_pns512.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/649610267" 
	@${RM} ${OBJECTDIR}/_ext/649610267/reader_pns512.o.d 
	@${RM} ${OBJECTDIR}/_ext/649610267/reader_pns512.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/649610267/reader_pns512.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/649610267/reader_pns512.o.d" -o ${OBJECTDIR}/_ext/649610267/reader_pns512.o ../src/cardReaderEmulator/src/reader/reader_pns512.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o: ../src/cardReaderEmulator/src/reader/reader_uart_cmds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/649610267" 
	@${RM} ${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o.d 
	@${RM} ${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o.d" -o ${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o ../src/cardReaderEmulator/src/reader/reader_uart_cmds.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/649610267/registers.o: ../src/cardReaderEmulator/src/reader/registers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/649610267" 
	@${RM} ${OBJECTDIR}/_ext/649610267/registers.o.d 
	@${RM} ${OBJECTDIR}/_ext/649610267/registers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/649610267/registers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/649610267/registers.o.d" -o ${OBJECTDIR}/_ext/649610267/registers.o ../src/cardReaderEmulator/src/reader/registers.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/986819774/tag_ntag213.o: ../src/cardReaderEmulator/src/tag/tag_ntag213.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/986819774" 
	@${RM} ${OBJECTDIR}/_ext/986819774/tag_ntag213.o.d 
	@${RM} ${OBJECTDIR}/_ext/986819774/tag_ntag213.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/986819774/tag_ntag213.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/986819774/tag_ntag213.o.d" -o ${OBJECTDIR}/_ext/986819774/tag_ntag213.o ../src/cardReaderEmulator/src/tag/tag_ntag213.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o: ../src/cardReaderEmulator/src/tag/tag_uart_cmds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/986819774" 
	@${RM} ${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o.d 
	@${RM} ${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o.d" -o ${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o ../src/cardReaderEmulator/src/tag/tag_uart_cmds.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o: ../src/cardReaderEmulator/src/reader_tag_io_buffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1214191177" 
	@${RM} ${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o.d" -o ${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o ../src/cardReaderEmulator/src/reader_tag_io_buffer.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1214191177/uart_cmds.o: ../src/cardReaderEmulator/src/uart_cmds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1214191177" 
	@${RM} ${OBJECTDIR}/_ext/1214191177/uart_cmds.o.d 
	@${RM} ${OBJECTDIR}/_ext/1214191177/uart_cmds.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1214191177/uart_cmds.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1214191177/uart_cmds.o.d" -o ${OBJECTDIR}/_ext/1214191177/uart_cmds.o ../src/cardReaderEmulator/src/uart_cmds.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/54334336/nature_myTag.o: ../src/pageSamples/nature_myTag.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/54334336" 
	@${RM} ${OBJECTDIR}/_ext/54334336/nature_myTag.o.d 
	@${RM} ${OBJECTDIR}/_ext/54334336/nature_myTag.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/54334336/nature_myTag.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/54334336/nature_myTag.o.d" -o ${OBJECTDIR}/_ext/54334336/nature_myTag.o ../src/pageSamples/nature_myTag.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/54334336/test2.o: ../src/pageSamples/test2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/54334336" 
	@${RM} ${OBJECTDIR}/_ext/54334336/test2.o.d 
	@${RM} ${OBJECTDIR}/_ext/54334336/test2.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/54334336/test2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/54334336/test2.o.d" -o ${OBJECTDIR}/_ext/54334336/test2.o ../src/pageSamples/test2.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/384435606/sys_clk_static.o: ../src/system_config/PIC32MX250F128B/framework/system/clk/src/sys_clk_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/384435606" 
	@${RM} ${OBJECTDIR}/_ext/384435606/sys_clk_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/384435606/sys_clk_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/384435606/sys_clk_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/384435606/sys_clk_static.o.d" -o ${OBJECTDIR}/_ext/384435606/sys_clk_static.o ../src/system_config/PIC32MX250F128B/framework/system/clk/src/sys_clk_static.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1417152314/sys_ports_static.o: ../src/system_config/PIC32MX250F128B/framework/system/ports/src/sys_ports_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1417152314" 
	@${RM} ${OBJECTDIR}/_ext/1417152314/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/1417152314/sys_ports_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1417152314/sys_ports_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1417152314/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/1417152314/sys_ports_static.o ../src/system_config/PIC32MX250F128B/framework/system/ports/src/sys_ports_static.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/324476799/system_init.o: ../src/system_config/PIC32MX250F128B/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/324476799" 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/324476799/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/324476799/system_init.o.d" -o ${OBJECTDIR}/_ext/324476799/system_init.o ../src/system_config/PIC32MX250F128B/system_init.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/324476799/system_interrupt.o: ../src/system_config/PIC32MX250F128B/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/324476799" 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/324476799/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/324476799/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/324476799/system_interrupt.o ../src/system_config/PIC32MX250F128B/system_interrupt.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/324476799/system_exceptions.o: ../src/system_config/PIC32MX250F128B/system_exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/324476799" 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/324476799/system_exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/324476799/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/324476799/system_exceptions.o ../src/system_config/PIC32MX250F128B/system_exceptions.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/324476799/system_tasks.o: ../src/system_config/PIC32MX250F128B/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/324476799" 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/324476799/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/324476799/system_tasks.o.d" -o ${OBJECTDIR}/_ext/324476799/system_tasks.o ../src/system_config/PIC32MX250F128B/system_tasks.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o: ../src/usb/UART_CDC_Link.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659870090" 
	@${RM} ${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o.d 
	@${RM} ${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o.d" -o ${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o ../src/usb/UART_CDC_Link.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o: ../src/usb/USBCDC_wrapper.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659870090" 
	@${RM} ${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o.d 
	@${RM} ${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o.d" -o ${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o ../src/usb/USBCDC_wrapper.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/app.o: ../src/app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/app.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1360937237/app.o.d" -o ${OBJECTDIR}/_ext/1360937237/app.o ../src/app.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/tag_eeprom.o: ../src/tag_eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/tag_eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/tag_eeprom.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/tag_eeprom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1360937237/tag_eeprom.o.d" -o ${OBJECTDIR}/_ext/1360937237/tag_eeprom.o ../src/tag_eeprom.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/280795049/drv_i2c.o: ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/280795049" 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o.d 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" -o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/185269848/drv_tmr.o: ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/185269848" 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/764219029/drv_usbfs.o: ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/764219029" 
	@${RM} ${OBJECTDIR}/_ext/764219029/drv_usbfs.o.d 
	@${RM} ${OBJECTDIR}/_ext/764219029/drv_usbfs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/764219029/drv_usbfs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/764219029/drv_usbfs.o.d" -o ${OBJECTDIR}/_ext/764219029/drv_usbfs.o ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o: ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/764219029" 
	@${RM} ${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o.d" -o ${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs_device.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1271179505/sys_devcon.o: ../../../../framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1271179505" 
	@${RM} ${OBJECTDIR}/_ext/1271179505/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/1271179505/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1271179505/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1271179505/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/1271179505/sys_devcon.o ../../../../framework/system/devcon/src/sys_devcon.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o: ../../../../framework/system/devcon/src/sys_devcon_pic32mx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1271179505" 
	@${RM} ${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o.d 
	@${RM} ${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o.d" -o ${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o ../../../../framework/system/devcon/src/sys_devcon_pic32mx.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/122796885/sys_int_pic32.o: ../../../../framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/122796885" 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ../../../../framework/system/int/src/sys_int_pic32.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/813494389/sys_reset.o: ../../../../framework/system/reset/src/sys_reset.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/813494389" 
	@${RM} ${OBJECTDIR}/_ext/813494389/sys_reset.o.d 
	@${RM} ${OBJECTDIR}/_ext/813494389/sys_reset.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/813494389/sys_reset.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/813494389/sys_reset.o.d" -o ${OBJECTDIR}/_ext/813494389/sys_reset.o ../../../../framework/system/reset/src/sys_reset.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/610166344/usb_device.o: ../../../../framework/usb/src/dynamic/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device.o ../../../../framework/usb/src/dynamic/usb_device.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/610166344/usb_device_cdc.o: ../../../../framework/usb/src/dynamic/usb_device_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o ../../../../framework/usb/src/dynamic/usb_device_cdc.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o: ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o: ../src/cardReaderEmulator/src/3party/iso14443a_crc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1143853589" 
	@${RM} ${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o.d" -o ${OBJECTDIR}/_ext/1143853589/iso14443a_crc.o ../src/cardReaderEmulator/src/3party/iso14443a_crc.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/649610267/reader_pns512.o: ../src/cardReaderEmulator/src/reader/reader_pns512.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/649610267" 
	@${RM} ${OBJECTDIR}/_ext/649610267/reader_pns512.o.d 
	@${RM} ${OBJECTDIR}/_ext/649610267/reader_pns512.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/649610267/reader_pns512.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/649610267/reader_pns512.o.d" -o ${OBJECTDIR}/_ext/649610267/reader_pns512.o ../src/cardReaderEmulator/src/reader/reader_pns512.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o: ../src/cardReaderEmulator/src/reader/reader_uart_cmds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/649610267" 
	@${RM} ${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o.d 
	@${RM} ${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o.d" -o ${OBJECTDIR}/_ext/649610267/reader_uart_cmds.o ../src/cardReaderEmulator/src/reader/reader_uart_cmds.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/649610267/registers.o: ../src/cardReaderEmulator/src/reader/registers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/649610267" 
	@${RM} ${OBJECTDIR}/_ext/649610267/registers.o.d 
	@${RM} ${OBJECTDIR}/_ext/649610267/registers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/649610267/registers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/649610267/registers.o.d" -o ${OBJECTDIR}/_ext/649610267/registers.o ../src/cardReaderEmulator/src/reader/registers.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/986819774/tag_ntag213.o: ../src/cardReaderEmulator/src/tag/tag_ntag213.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/986819774" 
	@${RM} ${OBJECTDIR}/_ext/986819774/tag_ntag213.o.d 
	@${RM} ${OBJECTDIR}/_ext/986819774/tag_ntag213.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/986819774/tag_ntag213.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/986819774/tag_ntag213.o.d" -o ${OBJECTDIR}/_ext/986819774/tag_ntag213.o ../src/cardReaderEmulator/src/tag/tag_ntag213.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o: ../src/cardReaderEmulator/src/tag/tag_uart_cmds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/986819774" 
	@${RM} ${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o.d 
	@${RM} ${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o.d" -o ${OBJECTDIR}/_ext/986819774/tag_uart_cmds.o ../src/cardReaderEmulator/src/tag/tag_uart_cmds.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o: ../src/cardReaderEmulator/src/reader_tag_io_buffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1214191177" 
	@${RM} ${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o.d" -o ${OBJECTDIR}/_ext/1214191177/reader_tag_io_buffer.o ../src/cardReaderEmulator/src/reader_tag_io_buffer.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1214191177/uart_cmds.o: ../src/cardReaderEmulator/src/uart_cmds.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1214191177" 
	@${RM} ${OBJECTDIR}/_ext/1214191177/uart_cmds.o.d 
	@${RM} ${OBJECTDIR}/_ext/1214191177/uart_cmds.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1214191177/uart_cmds.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1214191177/uart_cmds.o.d" -o ${OBJECTDIR}/_ext/1214191177/uart_cmds.o ../src/cardReaderEmulator/src/uart_cmds.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/54334336/nature_myTag.o: ../src/pageSamples/nature_myTag.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/54334336" 
	@${RM} ${OBJECTDIR}/_ext/54334336/nature_myTag.o.d 
	@${RM} ${OBJECTDIR}/_ext/54334336/nature_myTag.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/54334336/nature_myTag.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/54334336/nature_myTag.o.d" -o ${OBJECTDIR}/_ext/54334336/nature_myTag.o ../src/pageSamples/nature_myTag.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/54334336/test2.o: ../src/pageSamples/test2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/54334336" 
	@${RM} ${OBJECTDIR}/_ext/54334336/test2.o.d 
	@${RM} ${OBJECTDIR}/_ext/54334336/test2.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/54334336/test2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/54334336/test2.o.d" -o ${OBJECTDIR}/_ext/54334336/test2.o ../src/pageSamples/test2.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/384435606/sys_clk_static.o: ../src/system_config/PIC32MX250F128B/framework/system/clk/src/sys_clk_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/384435606" 
	@${RM} ${OBJECTDIR}/_ext/384435606/sys_clk_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/384435606/sys_clk_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/384435606/sys_clk_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/384435606/sys_clk_static.o.d" -o ${OBJECTDIR}/_ext/384435606/sys_clk_static.o ../src/system_config/PIC32MX250F128B/framework/system/clk/src/sys_clk_static.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1417152314/sys_ports_static.o: ../src/system_config/PIC32MX250F128B/framework/system/ports/src/sys_ports_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1417152314" 
	@${RM} ${OBJECTDIR}/_ext/1417152314/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/1417152314/sys_ports_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1417152314/sys_ports_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1417152314/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/1417152314/sys_ports_static.o ../src/system_config/PIC32MX250F128B/framework/system/ports/src/sys_ports_static.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/324476799/system_init.o: ../src/system_config/PIC32MX250F128B/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/324476799" 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/324476799/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/324476799/system_init.o.d" -o ${OBJECTDIR}/_ext/324476799/system_init.o ../src/system_config/PIC32MX250F128B/system_init.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/324476799/system_interrupt.o: ../src/system_config/PIC32MX250F128B/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/324476799" 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/324476799/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/324476799/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/324476799/system_interrupt.o ../src/system_config/PIC32MX250F128B/system_interrupt.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/324476799/system_exceptions.o: ../src/system_config/PIC32MX250F128B/system_exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/324476799" 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/324476799/system_exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/324476799/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/324476799/system_exceptions.o ../src/system_config/PIC32MX250F128B/system_exceptions.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/324476799/system_tasks.o: ../src/system_config/PIC32MX250F128B/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/324476799" 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/324476799/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/324476799/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/324476799/system_tasks.o.d" -o ${OBJECTDIR}/_ext/324476799/system_tasks.o ../src/system_config/PIC32MX250F128B/system_tasks.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o: ../src/usb/UART_CDC_Link.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659870090" 
	@${RM} ${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o.d 
	@${RM} ${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o.d" -o ${OBJECTDIR}/_ext/659870090/UART_CDC_Link.o ../src/usb/UART_CDC_Link.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o: ../src/usb/USBCDC_wrapper.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/659870090" 
	@${RM} ${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o.d 
	@${RM} ${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o.d" -o ${OBJECTDIR}/_ext/659870090/USBCDC_wrapper.o ../src/usb/USBCDC_wrapper.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/app.o: ../src/app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/app.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1360937237/app.o.d" -o ${OBJECTDIR}/_ext/1360937237/app.o ../src/app.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/tag_eeprom.o: ../src/tag_eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/tag_eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/tag_eeprom.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/tag_eeprom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1360937237/tag_eeprom.o.d" -o ${OBJECTDIR}/_ext/1360937237/tag_eeprom.o ../src/tag_eeprom.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/280795049/drv_i2c.o: ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/280795049" 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o.d 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" -o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/185269848/drv_tmr.o: ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/185269848" 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/764219029/drv_usbfs.o: ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/764219029" 
	@${RM} ${OBJECTDIR}/_ext/764219029/drv_usbfs.o.d 
	@${RM} ${OBJECTDIR}/_ext/764219029/drv_usbfs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/764219029/drv_usbfs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/764219029/drv_usbfs.o.d" -o ${OBJECTDIR}/_ext/764219029/drv_usbfs.o ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o: ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/764219029" 
	@${RM} ${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o.d" -o ${OBJECTDIR}/_ext/764219029/drv_usbfs_device.o ../../../../framework/driver/usb/usbfs/src/dynamic/drv_usbfs_device.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1271179505/sys_devcon.o: ../../../../framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1271179505" 
	@${RM} ${OBJECTDIR}/_ext/1271179505/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/1271179505/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1271179505/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1271179505/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/1271179505/sys_devcon.o ../../../../framework/system/devcon/src/sys_devcon.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o: ../../../../framework/system/devcon/src/sys_devcon_pic32mx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1271179505" 
	@${RM} ${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o.d 
	@${RM} ${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o.d" -o ${OBJECTDIR}/_ext/1271179505/sys_devcon_pic32mx.o ../../../../framework/system/devcon/src/sys_devcon_pic32mx.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/122796885/sys_int_pic32.o: ../../../../framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/122796885" 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ../../../../framework/system/int/src/sys_int_pic32.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/813494389/sys_reset.o: ../../../../framework/system/reset/src/sys_reset.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/813494389" 
	@${RM} ${OBJECTDIR}/_ext/813494389/sys_reset.o.d 
	@${RM} ${OBJECTDIR}/_ext/813494389/sys_reset.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/813494389/sys_reset.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/813494389/sys_reset.o.d" -o ${OBJECTDIR}/_ext/813494389/sys_reset.o ../../../../framework/system/reset/src/sys_reset.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/610166344/usb_device.o: ../../../../framework/usb/src/dynamic/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device.o ../../../../framework/usb/src/dynamic/usb_device.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/610166344/usb_device_cdc.o: ../../../../framework/usb/src/dynamic/usb_device_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device_cdc.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device_cdc.o ../../../../framework/usb/src/dynamic/usb_device_cdc.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o: ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/PIC32MX250F128B" -I"../src/PIC32MX250F128B" -I"../../../../framework" -I"../src/system_config/PIC32MX250F128B/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_device_cdc_acm.o ../../../../framework/usb/src/dynamic/usb_device_cdc_acm.c    -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/vTagReader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../bin/framework/peripheral/PIC32MX250F128B_peripherals.a  ../src/system_config/PIC32MX250F128B/app_mx.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/vTagReader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ..\..\..\..\bin\framework\peripheral\PIC32MX250F128B_peripherals.a      -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC00490:0x1FC00BEF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_PK3=1,--defsym=_min_heap_size=0,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/vTagReader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../bin/framework/peripheral/PIC32MX250F128B_peripherals.a ../src/system_config/PIC32MX250F128B/app_mx.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/vTagReader.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ..\..\..\..\bin\framework\peripheral\PIC32MX250F128B_peripherals.a      -DXPRJ_PIC32MX250F128B=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=0,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/vTagReader.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/PIC32MX250F128B
	${RM} -r dist/PIC32MX250F128B

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
