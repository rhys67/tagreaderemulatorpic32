 /**
    PN512 and NTAG213 Emulator 
    Copyright (C) 2017  Rhys Bryant

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

  ********************************************************************************
  MPLAB Harmony Application Source File
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************
#include <stdio.h>
#include "app.h"
#include "cardReaderEmulator/src/reader/reader_pns512.h"
#include "cardReaderEmulator/src/reader/reader_timer.h"
#include "cardReaderEmulator/src/reader_tag_io_buffer.h"
#include "cardReaderEmulator/src/tag/tag_eeprom.h"
#include "cardReaderEmulator/src/tag/tag_ntag213.h"
#include "cardReaderEmulator/src/reader_config.h"
#include "cardReaderEmulator/src/uptime_time.h"
// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/
DRV_HANDLE huart;
APP_DATA appData;
PNSReader reader;
uint64_t last_led_toggle=0;
#define PN512_TIMER_CLOCK (6800000)
#define EMULATED_PN512_TIMER_CLOCK 250000
#define TIMER_COUNTER_INCREMENT (PN512_TIMER_CLOCK/EMULATED_PN512_TIMER_CLOCK)
void APP_printEvent(DRV_I2C_BUFFER_EVENT event);
void uart_write_response(char* responseData, short len);
static void APP_USBCommandCDC_DataReceiveCompleted(void* device);
static void APP_USBCommandCDC_writeResponse(char* responseData, short len);

void APP_I2CBufferEventFunction( DRV_I2C_BUFFER_EVENT event,
                                 DRV_I2C_BUFFER_HANDLE handle, 
                                 uintptr_t context)
{
    switch(event)
    {
        case DRV_I2C_BUFFER_SLAVE_READ_REQUESTED:
            ////printf("e");
            break;
        case DRV_I2C_BUFFER_EVENT_COMPLETE:

        break;
        case  DRV_I2C_BUFFER_SLAVE_READ_BYTE:
            if(appData.currentRequest.gotAddress && appData.currentRequest.lastI2cOperation != I2C_REQUEST_TYPE_WRITE){
                reader_pns512_reg_request_write(&reader,appData.i2cReadBuffer[0],appData.i2cReadBuffer + 1);
                
            }
            appData.currentRequest.regAddress=appData.i2cReadBuffer[0];
            appData.currentRequest.gotAddress=true;
            appData.currentRequest.lastI2cOperation=I2C_REQUEST_TYPE_READ;
        break;
        //case DRV_I2C_BUFFER_EVENT_ERROR:
            // Error handling here.
        //break;

        default:
            break;
    }
}


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void timer_callback( uintptr_t context, uint32_t alarmCount ){
    //
    //PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_A,PORTS_BIT_POS_4 );

    reader.timer.counter-=TIMER_COUNTER_INCREMENT;

    if(  reader.timer.counter<= 0){
      
        reader_timer_finished(&reader);
        reader.timer.counter=reader.timer.reload;
        
        if( ! reader.timer.autoRestart){
            reader_timer_stop(NULL);
        }
        
    }
    
   
}
uint64_t uptime_get_ms(){
    return appData.upTimeCounter;
}
void uptime_timer_callback( uintptr_t context, uint32_t alarmCount ){
    LATBbits.LATB4=(~LATBbits.LATB4);
    appData.upTimeCounter++;
}

uint32_t reader_timer_getUptime(){
    return appData.upTimeCounter;
}
// Event is received when
// the buffer is processed.
void I2CMasterOpStatusCb ( DRV_I2C_BUFFER_EVENT event,
                           DRV_I2C_BUFFER_HANDLE bufferHandle,
                           uintptr_t context){
    
    switch(event){
        case DRV_I2C_BUFFER_EVENT_COMPLETE:
        {
            Nop();
            
            if (bufferHandle == appData.hTagDataEEPROMI2c){
            // appData.state=EEPROM_READ_COMPLETE;
             //tag_eeprom_read_page_completed(int8_t errCode,int8_t pageNum,uint8_t* pageDataBuf,int8_t bufSize);
             
             appData.hTagDataEEPROMI2c = NULL;
             uint8_t startByteIndexRequested=appData.EEPROMI2cRequestBuffer[1];
             if(appData.lastEEpromRequest==I2C_REQUEST_TYPE_READ){
                 appData.lastEEpromRequest=I2C_REQUEST_TYPE_NONE;
                tag_eeprom_read_page_completed(TAG_EEPROM_ERROR_OK,appData.EEPROM_lastTagPageRequested,(char*)appData.EEPROMI2cResponseBuffer,16);
             }else{
                 appData.lastEEpromRequest=I2C_REQUEST_TYPE_NONE;
                tag_eeprom_Write_completed(TAG_EEPROM_ERROR_OK,appData.EEPROM_lastTagPageRequested);
             }
             
            }
            else if( bufferHandle == appData.hEEPROMConfigBuffer){
                
                
                appData.EEPROMI2cConfigCallback(1,
                        appData.EEPROMI2cConfigBuffer[0],
                        &reader);
                appData.hEEPROMConfigBuffer=NULL;
            }
            else if(bufferHandle == appData.hEEPROMConfigSetBuffer){
                if( appData.EEPROMI2cConfigSavedCallback != NULL )
                    appData.EEPROMI2cConfigSavedCallback(TAG_EEPROM_ERROR_OK);
                appData.hEEPROMConfigSetBuffer=NULL;
            }
            
            break;
        }
    }
  
}

static void tag_data_load_completed(int8_t result,void* userData){
    threedprinterUitls_updateTagUID();
}

void APP_Initialize ( void )
{
    appData.readerCommandCDC.onDataReceiveCompleted=APP_USBCommandCDC_DataReceiveCompleted;
    appData.readerCommandCDCHandler.writeResponse=APP_USBCommandCDC_writeResponse;
    appData.readerCommandCDCHandler.reader=&reader;
    appData.readerCommandCDC.onLineCodingChange=0;
    appData.readerCommandCDC.onDataSendCompleted=0;
    appData.readPending=true;
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
//    huart=DRV_USART_Open(0,DRV_IO_INTENT_WRITE);
    
   
    
    appData.hI2cSlave = DRV_I2C_Open(DRV_I2C_INDEX_0, DRV_IO_INTENT_READWRITE);
    appData.hEEPROMI2c = DRV_I2C_Open(DRV_I2C_INDEX_1, DRV_IO_INTENT_READWRITE);
    appData.hUptimeTimer =DRV_TMR_Open(DRV_TMR_INDEX_1,DRV_IO_INTENT_EXCLUSIVE ); 
    appData.hReaderTimer =DRV_TMR_Open(DRV_TMR_INDEX_0,DRV_IO_INTENT_EXCLUSIVE );
    appData.upTimeCounter =0;
    
    int timerFreq=DRV_TMR_CounterFrequencyGet(appData.hUptimeTimer);

    DRV_TMR_AlarmRegister(appData.hUptimeTimer,timerFreq/1000,
                    true,NULL,uptime_timer_callback);

    DRV_TMR_Start(appData.hUptimeTimer);
    
    DRV_I2C_BufferEventHandlerSet( appData.hI2cSlave, 
                               APP_I2CBufferEventFunction,
                               0 );
    
    DRV_I2C_BufferEventHandlerSet(appData.hEEPROMI2c, I2CMasterOpStatusCb, NULL );
    
    appData.currentRequest.regAddress = INVALID_REG_ADDRESS;
    appData.currentRequest.lastI2cOperation = I2C_REQUEST_TYPE_NONE;
    reader_pns512_init(&reader);
    appData.uartCmdHandler.currentCommand=0;
    appData.uartCmdHandler.reader=&reader;
    appData.uartCmdHandler.writeResponse=uart_write_response;
    
    
    
    tag_ntag213_init();
    tag_runtime_data.currentOperationCompletedCallback=tag_data_load_completed;

    
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */
 void APP_I2CSlaveCallback(DRV_I2C_BUFFER_EVENT event, void * context){
     switch(event){
         case DRV_I2C_SEND_STOP_EVENT:
            //printf("stop");
    /* Master sends data to slave */
         case  DRV_I2C_BUFFER_SLAVE_READ_REQUESTED:
            appData.hI2cReadBuffer = DRV_I2C_Receive(appData.hI2cSlave,0,&appData.i2cReadBuffer,I2C_READ_BUFFER_SIZE,NULL);
            appData.currentRequest.gotAddress=false;
            
             //printf("r");
           break;
    /* Master requests data from slave */
         case DRV_I2C_BUFFER_SLAVE_WRITE_REQUESTED:
              
              
              if( appData.currentRequest.regAddress != INVALID_REG_ADDRESS ){
                 /* Reg read request */
                reader_pns512_reg_request_read(&reader,appData.currentRequest.regAddress,appData.i2cWriteBuffer);
                appData.currentRequest.lastI2cOperation = I2C_REQUEST_TYPE_WRITE;
                appData.currentRequest.regAddress = INVALID_REG_ADDRESS;
              }
              appData.hI2cWriteBuffer = DRV_I2C_Transmit(appData.hI2cSlave,0,&appData.i2cWriteBuffer,I2C_WRITE_BUFFER_SIZE,NULL);
             break;
     }
}
 
 void APP_outputDebugBuffer(){
     char buffer[128]="";
     short nBytesRead=debug_buffer_read((char*)buffer,128);
     if( nBytesRead ){
        DRV_USART_Write(huart,buffer,nBytesRead);
     }
 }
 
 
int8_t tag_eeprom_write_page(int8_t pageNum,uint8_t* pageDataBuf,int8_t bufSize){
    if( bufSize > sizeof(appData.EEPROMI2cRequestBuffer) -1 ){
        return ;
    }
    
    /*if (appData.hTagDataEEPROMI2c != NULL){
        return TAG_EEPROM_ERROR_BUSY;
    }*/
   
    appData.EEPROMI2cRequestBuffer[0] = pageNum * NTAG_NUM_BYTES_PER_PAGE;
    memcpy(appData.EEPROMI2cRequestBuffer+1,pageDataBuf,bufSize);
    
    appData.lastEEpromRequest=I2C_REQUEST_TYPE_WRITE;
    
    DRV_I2C_StopEventSend(appData.hEEPROMI2c);
    appData.hTagDataEEPROMI2c = DRV_I2C_Transmit(appData.hEEPROMI2c,EEPROM_I2C_ADDRESS ,appData.EEPROMI2cRequestBuffer,bufSize+1,NULL);
   
   // return 0;
}

void tag_eeprom_read_pages(int8_t startPage){
    
    uint8_t readStartPos=startPage * NTAG_NUM_BYTES_PER_PAGE;
    uint8_t numBytesToRead=NTAG_NUM_BYTES_RETURNED_PER_READ;
    
    if( readStartPos + NTAG_NUM_BYTES_RETURNED_PER_READ > (NTAG_NUM_PAGES * NTAG_NUM_BYTES_PER_PAGE)){
        numBytesToRead=(NTAG_NUM_PAGES * NTAG_NUM_BYTES_PER_PAGE)-readStartPos;
    }
    appData.EEPROM_lastTagPageRequested=startPage;
    appData.EEPROMI2cRequestBuffer[0]=readStartPos;
    appData.lastEEpromRequest=I2C_REQUEST_TYPE_READ;
    appData.hTagDataEEPROMI2c 
            = DRV_I2C_TransmitThenReceive(appData.hEEPROMI2c,EEPROM_I2C_ADDRESS ,appData.EEPROMI2cRequestBuffer,
            1,appData.EEPROMI2cResponseBuffer,numBytesToRead,NULL);
}

void reader_config_get(int8_t configItemIndex,GetConfigCompleted callback,void* context){
    
     
    //config is stored at the end of the eeprom
    appData.EEPROMI2cConfigBuffer[0]=254-configItemIndex;

    appData.hEEPROMConfigBuffer =
    DRV_I2C_TransmitThenReceive(appData.hEEPROMI2c,EEPROM_I2C_ADDRESS ,appData.EEPROMI2cConfigBuffer,
            1,appData.EEPROMI2cConfigBuffer,1,NULL);
    appData.EEPROMI2cConfigCallback=callback;
}

void reader_config_set(int8_t configItemIndex,uint8_t data,SetConfigCompleted callback){
    //config is stored at the end of the eeprom
    appData.EEPROMI2cConfigBuffer[0]=253-configItemIndex;
    appData.EEPROMI2cConfigBuffer[1]=data;
    appData.EEPROMI2cConfigSavedCallback=callback;
    appData.hEEPROMConfigSetBuffer=DRV_I2C_Transmit(appData.hEEPROMI2c,EEPROM_I2C_ADDRESS ,appData.EEPROMI2cConfigBuffer,2,NULL);
}
void APP_reset_to_bootloader_flag_saved(int8_t errCode){
    if( errCode == TAG_EEPROM_ERROR_OK){
        appData.state=APP_STATE_SOFT_RESET;
    }
}
void reader_pns512_enter_upgrade_mode(){
    reader_config_set(-1,1| 128,APP_reset_to_bootloader_flag_saved);
}

void uart_write_response(char* responseData, short len){
    //DRV_USART_Write(huart,responseData,len);
}

void APP_readUart(){
    /*if(PLIB_USART_ReceiverDataIsAvailable(USART_ID_2)){
        char b=PLIB_USART_ReceiverByteReceive(USART_ID_2);
        uart_cmds_readByte(&appData.uartCmdHandler,b);
    }*/
}

void APP_USBCommandCDC_DataReceiveCompleted(void* device) {
    appData.readPending=false;
}

void APP_USBCommandCDC_writeResponse(char* responseData, short len){
    short i=0;
    appData.readerCommandCDC.TXBufPos=0;
    for(i=0;i< len;i++){
        appData.readerCommandCDC.TXBuf[appData.readerCommandCDC.TXBufPos++]=responseData[i];

    }
    USBCDC_sendBuffer(&appData.readerCommandCDC);
    
}
void APP_Tasks ( void )
{
     //LATAINV = ~4;
    /* read + read == Set Reg */
    /* read + write == Read Reg*/
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            USBCDC_Init(&appData.readerCommandCDC);
            appData.state = APP_STATE_SERVICE_TASKS;
            break;
        }
        case APP_STATE_SOFT_RESET:
//            SYS_RESET_SoftwareReset();
            break;
        case APP_STATE_START_TIMER:
        {

            appData.state=APP_STATE_SERVICE_TASKS;
            break;
        }
        case EEPROM_READ_COMPLETE:
        {
            uint8_t startByteIndexRequested=appData.EEPROMI2cRequestBuffer[1];
            
            tag_eeprom_read_page_completed(TAG_EEPROM_ERROR_OK,appData.EEPROM_lastTagPageRequested,(char*)appData.EEPROMI2cResponseBuffer,16);
            
            appData.state=APP_STATE_SERVICE_TASKS;
            break;
        }
        case APP_STATE_SERVICE_TASKS:
        {
            reader_pns512_process_TX_RX(&reader);
            appData.state=APP_STATE_CHECK_UART;
            
            if (appData.upTimeCounter-last_led_toggle>5000){
                LATBbits.LATB1 = ~(LATBbits.LATB1);
                last_led_toggle=appData.upTimeCounter;
            }

            break;
        }
        case APP_STATE_CHECK_UART:
        {
            APP_readUart();
            appData.state=APP_STATE_CHECK_CMD_CDC;
            break;
        }
        case APP_STATE_CHECK_CMD_CDC:
        {
            if( ! appData.readPending ){
                int extra=0;
                CDCDevice* cdcDevice = &appData.readerCommandCDC;
                /*
                 * workaround don't know why but when sending exactly 2 bytes
                 *  the CDC Driver returns a length of 1 to workround 
                 *  if the incoming command is known to require 2 bytes add an extra one to the length
                 */
                if( false && cdcDevice->RXBuf[0] == 66){
                    extra=1;
                }
                if (cdcDevice->RXBufTail != cdcDevice->RXBufHead) {
                    while (cdcDevice->RXBufTail < cdcDevice->RXBufHead+extra) {

                        char b=cdcDevice->RXBuf[cdcDevice->RXBufTail];
                        uart_cmds_readByte(&appData.readerCommandCDCHandler,b);
                        cdcDevice->RXBufTail++;
                    }
                    cdcDevice->RXBufTail=0;
                    cdcDevice->RXBufHead=0;
                    USBCDC_addRecBuffer(cdcDevice);
                    appData.readPending = true;
                }
            }
            
            appData.state=APP_STATE_SERVICE_TASKS;
            break;
        }

        /* TODO: implement your application state machine.*/
        

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}

void reader_timer_setTimerMode(ReaderTimer* timerContext,int timerMode){
    
}
void reader_timer_stop(ReaderTimer* timerContext){
    DRV_TMR_Stop(appData.hReaderTimer);
    DRV_TMR_AlarmDeregister(appData.hReaderTimer);
    
}
void reader_timer_start(ReaderTimer* timerContext){
    timerContext->counter=timerContext->reload;
    int timerFreq=DRV_TMR_CounterFrequencyGet(appData.hReaderTimer);

    DRV_TMR_AlarmRegister(appData.hReaderTimer,timerFreq/EMULATED_PN512_TIMER_CLOCK,
                    true,NULL,timer_callback);

    DRV_TMR_Start(appData.hReaderTimer);
    
}
