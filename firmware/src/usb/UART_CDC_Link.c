 /**
    PN512 and NTAG213 Emulator 
    Copyright (C) 2017  Rhys Bryant

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
#include "system_definitions.h"
#include "../cardReaderEmulator/src/3party/circular_buffer.h"
#include "USBCDC_wrapper.h"
#include <stdint.h>
#include <stdbool.h>
#include "UART_CDC_Link.h"

/*
 * fill the UART recive Buffer 
 */
bool readUart(UART_CDC* uart) {
    bool hasData = false;
    while (PLIB_USART_ReceiverDataIsAvailable(uart->deviceIndex)) {
        char b = PLIB_USART_ReceiverByteReceive(uart->deviceIndex);
        if (!circular_buffer_circBufPush(&uart->RX_FIFO_buffer, b)) {
            break;
        }
        hasData = true;
    }

    return hasData;
}

/*
 * send out data in the uart buffer
 */
void writeUART(UART_CDC* uart) {
    uint8_t data;
    while (!PLIB_USART_TransmitterBufferIsFull(uart->deviceIndex) && !circular_buffer_circBufPop(&uart->TX_FIFO_buffer, &data)) {
        PLIB_USART_TransmitterByteSend(uart->deviceIndex, data);
    }
}

/**
 * copy data from the RX CDC buffer into the UART Buffer
 * @param uart
 */
void USBCDC_UART_Send(UART_CDC* uart) {
    if (uart->cdcDevice.RXBufTail != uart->cdcDevice.RXBufHead) {
        while (uart->cdcDevice.RXBufTail < uart->cdcDevice.RXBufHead) {
            if (circular_buffer_circBufPush(&uart->TX_FIFO_buffer, uart->cdcDevice.RXBuf[uart->cdcDevice.RXBufTail])) {
                break;
            }
            uart->cdcDevice.RXBufTail++;
        }
    }
}

/*
 * copy data from the UART RX Buffer into the CDC TX Buffer
 */
bool USBCDC_UART_Recive(UART_CDC* uartCDC) {
    uint8_t data;
    bool dataWasRead = false;
    while (!circular_buffer_circBufPop(&uartCDC->RX_FIFO_buffer, &data)) {
        uartCDC->cdcDevice.TXBuf[uartCDC->cdcDevice.TXBufPos++] = data;
        dataWasRead = true;
    }
    return dataWasRead;
}

/*
 * UART RX/TX tasks
 */
void UART_CDC_Link_Tasks(UART_CDC* uartCDC) {
    switch (uartCDC->state) {
        case UART_CDC_LINK_CHECK_TX:
            writeUART(uartCDC);
            uartCDC->state = UART_CDC_LINK_CHECK_RX;
            break;
        case UART_CDC_LINK_CHECK_RX:
            readUart(uartCDC);
            uartCDC->state = UART_CDC_SCHEDULE_SEND;
            break;
        case UART_CDC_SCHEDULE_SEND:
            if (!uartCDC->CDCsendPending && uartCDC->cdcDevice.isConnected) {
                if (USBCDC_UART_Recive(uartCDC)) {
                    USBCDC_sendBuffer(&uartCDC->cdcDevice);
                }
            }
            uartCDC->state = UART_CDC_SCHEDULE_RECIVE;
            break;
        case UART_CDC_SCHEDULE_RECIVE:
            if (!uartCDC->CDCRecivePending && uartCDC->cdcDevice.isConnected) {
                USBCDC_UART_Send(uartCDC);
                if (uartCDC->cdcDevice.RXBufTail == uartCDC->cdcDevice.RXBufHead) {
                    uartCDC->cdcDevice.RXBufTail = 0;
                    uartCDC->cdcDevice.RXBufHead = 0;
                    USBCDC_addRecBuffer(&uartCDC->cdcDevice);
                    uartCDC->CDCRecivePending = true;

                }
            }
            uartCDC->state = UART_CDC_LINK_CHECK_TX;
            break;
    }
}

void dataReceiveCompleted(void* device) {
    CDCDevice* dev = (CDCDevice*) device;
    UART_CDC* uartCDC = ((UART_CDC*) dev->userData);
    uartCDC->CDCRecivePending = false;

    if (false && !uartCDC->CDCRecivePending && uartCDC->cdcDevice.isConnected) {
        USBCDC_UART_Send(uartCDC);
        if (uartCDC->cdcDevice.RXBufTail == uartCDC->cdcDevice.RXBufHead) {
            uartCDC->cdcDevice.RXBufTail = 0;
            uartCDC->cdcDevice.RXBufHead = 0;
            USBCDC_addRecBuffer(&uartCDC->cdcDevice);
            uartCDC->CDCRecivePending = true;

        }
    }
}

void dataSendCompleted(void* device) {
    CDCDevice* dev = (CDCDevice*) device;
    ((UART_CDC*) dev->userData)->CDCsendPending = false;
}

/**
 * allows the USB CDC end to set the uart bandrate
 * @param device
 */
void lineCodingChanged(void* device) {
    CDCDevice* dev = (CDCDevice*) device;
    short uartDevIndex = ((UART_CDC*) dev->userData)->deviceIndex;
    int clockSource = SYS_CLK_PeripheralFrequencyGet(CLK_BUS_PERIPHERAL_1);

    PLIB_USART_BaudSetAndEnable(uartDevIndex, clockSource, dev->lineCodingData.dwDTERate);

}

void UART_CDC_Link_Init(UART_CDC* uartCDC) {
    uartCDC->cdcDevice.onDataReceiveCompleted = dataReceiveCompleted;
    uartCDC->cdcDevice.onDataSendCompleted = dataSendCompleted;
    uartCDC->cdcDevice.onLineCodingChange = lineCodingChanged;
    uartCDC->cdcDevice.userData = uartCDC;
    uartCDC->cdcDevice.lineCodingData.dwDTERate = 9600;
    uartCDC->CDCRecivePending = false;
    uartCDC->CDCsendPending = false;
}