#ifndef USBCDC_WRAPPER_H
#define USBCDC_WRAPPER_H
#define USBCDC_BUFFER_SIZE 64

typedef void (*USBCDC_WRAPPER_CALLBACK) (void* device);

typedef struct{
    USB_DEVICE_HANDLE hDevice;
    // Control Line State 
    USB_CDC_CONTROL_LINE_STATE controlLineStateData;

    // Line Coding Data 
    USB_CDC_LINE_CODING lineCodingData;
    
    uint8_t deviceIndex;
    char TXBuf[USBCDC_BUFFER_SIZE];
    char RXBuf[USBCDC_BUFFER_SIZE];
    uint8_t TXBufPos;
    uint8_t RXBufHead;
    uint8_t RXBufTail;
    USB_DEVICE_CDC_TRANSFER_HANDLE hTXBuffer;
    USB_DEVICE_CDC_TRANSFER_HANDLE hRXBuffer;
    void* userData;
    USBCDC_WRAPPER_CALLBACK onDataSendCompleted;
    USBCDC_WRAPPER_CALLBACK onDataReceiveCompleted;
    USBCDC_WRAPPER_CALLBACK onLineCodingChange;
    bool isConnected;
} CDCDevice;

inline void USBCDC_sendBuffer(CDCDevice* d){
    USB_DEVICE_CDC_Write(d->deviceIndex,&d->hTXBuffer,d->TXBuf,d->TXBufPos,0);
    
}

inline void USBCDC_addRecBuffer(CDCDevice* d){
    //schedule the next read
        USB_DEVICE_CDC_Read(d->deviceIndex,&d->hRXBuffer,d->RXBuf+d->RXBufHead,USBCDC_BUFFER_SIZE);
}

void USBCDC_Init(CDCDevice* d);
#endif