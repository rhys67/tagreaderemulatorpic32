 /**
    PN512 and NTAG213 Emulator 
    Copyright (C) 2017  Rhys Bryant

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
#include "system_definitions.h"
#include "USBCDC_wrapper.h"

USB_DEVICE_CDC_EVENT_RESPONSE APP_USBDeviceCDCEventHandler 
(
    USB_DEVICE_CDC_INDEX instanceIndex ,
    USB_DEVICE_CDC_EVENT event ,
    void* pData, 
    uintptr_t context 
)
{
    CDCDevice* deviceInfo=(CDCDevice*)context;
    
    switch(event) 
    {
        case USB_DEVICE_CDC_EVENT_GET_LINE_CODING:
                // This means the host wants to know the current line
                // coding. This is a control transfer request. Use the
                // USB_DEVICE_ControlSend() function to send the data to
                // host.  

                USB_DEVICE_ControlSend(deviceInfo->hDevice,
                    &deviceInfo->lineCodingData, sizeof(USB_CDC_LINE_CODING));

        break;
        
        case USB_DEVICE_CDC_EVENT_SET_LINE_CODING:

            // This means the host wants to set the line coding.
            // This is a control transfer request. Use the
            // USB_DEVICE_ControlReceive() function to receive the
            // data from the host 

            USB_DEVICE_ControlReceive(deviceInfo->hDevice,
                    &deviceInfo->lineCodingData, sizeof(USB_CDC_LINE_CODING));
            
            if( deviceInfo->onLineCodingChange ){
                //deviceInfo->onLineCodingChange(deviceInfo);
            }

        break;
        
        case USB_DEVICE_CDC_EVENT_SET_CONTROL_LINE_STATE:

            // This means the host is setting the control line state.
            // Read the control line state. We will accept this request
            // for now.
            deviceInfo->controlLineStateData.dtr = ((USB_CDC_CONTROL_LINE_STATE *)pData)->dtr; 
            deviceInfo->controlLineStateData.carrier = ((USB_CDC_CONTROL_LINE_STATE *)pData)->carrier; 
            USB_DEVICE_ControlStatus(deviceInfo->hDevice, USB_DEVICE_CONTROL_STATUS_OK);

        break;
        
        case USB_DEVICE_CDC_EVENT_CONTROL_TRANSFER_DATA_RECEIVED:

            // The data stage of the last control transfer is
            // complete. For now we accept all the data 

            USB_DEVICE_ControlStatus(deviceInfo->hDevice, USB_DEVICE_CONTROL_STATUS_OK);
            
        break;
        
            case USB_DEVICE_CDC_EVENT_CONTROL_TRANSFER_DATA_SENT:

            // This means the GET LINE CODING function data is valid. We dont
            // do much with this data in this demo. 
        break;
        
        case USB_DEVICE_CDC_EVENT_SEND_BREAK:

            // This means that the host is requesting that a break of the
            // specified duration be sent. 
            USB_DEVICE_ControlStatus(deviceInfo->hDevice, USB_DEVICE_CONTROL_STATUS_OK);    
        
        break;
      
        case USB_DEVICE_CDC_EVENT_READ_COMPLETE:
        {
            // This means that the host has sent some data
            USB_DEVICE_CDC_EVENT_DATA_READ_COMPLETE* eData=(USB_DEVICE_CDC_EVENT_DATA_READ_COMPLETE*)  pData;
            deviceInfo->RXBufHead+=eData->length;
            
            deviceInfo->onDataReceiveCompleted(deviceInfo);
            
            break;
        }
        case USB_DEVICE_CDC_EVENT_WRITE_COMPLETE:
            // This means that the host has sent some data
            if( deviceInfo->onDataSendCompleted  )
            deviceInfo->onDataSendCompleted(deviceInfo);
            deviceInfo->TXBufPos=0;
            
            break;
        
        default:
            break; 
    }

    return USB_DEVICE_CDC_EVENT_RESPONSE_NONE;
}

USB_DEVICE_EVENT_RESPONSE APP_USBDeviceEventHandler
(
    USB_DEVICE_EVENT event,
    void * pData, 
    uintptr_t context
)
{
    CDCDevice* deviceInfo=(CDCDevice*)context;
    USB_SETUP_PACKET * setupPacket;
    switch(event)
    {
        case USB_DEVICE_EVENT_POWER_DETECTED:
            // This event in generated when VBUS is detected. Attach the device 
            USB_DEVICE_Attach(deviceInfo->hDevice);
            break;
            
        case USB_DEVICE_EVENT_POWER_REMOVED:
            // This event is generated when VBUS is removed. Detach the device
            USB_DEVICE_Detach (deviceInfo->hDevice);
            deviceInfo->isConnected=false;
            break; 
            
        case USB_DEVICE_EVENT_CONFIGURED:
            // This event indicates that Host has set Configuration in the Device. 
            // Register CDC Function driver Event Handler.  
            USB_DEVICE_CDC_EventHandlerSet(USB_DEVICE_CDC_INDEX_0, APP_USBDeviceCDCEventHandler, (uintptr_t)deviceInfo);
            
            USB_DEVICE_CDC_Read(deviceInfo->deviceIndex,&deviceInfo->hRXBuffer,deviceInfo->RXBuf,USBCDC_BUFFER_SIZE);
            deviceInfo->isConnected=true;
            break;
            
        case USB_DEVICE_EVENT_CONTROL_TRANSFER_SETUP_REQUEST:
            // This event indicates a Control transfer setup stage has been completed. 
            setupPacket = (USB_SETUP_PACKET *)pData;
            
            // Parse the setup packet and respond with a USB_DEVICE_ControlSend(), 
            // USB_DEVICE_ControlReceive or USB_DEVICE_ControlStatus(). 
            
            break; 
            
        case USB_DEVICE_EVENT_CONTROL_TRANSFER_DATA_SENT:
            // This event indicates that a Control transfer Data has been sent to Host.   
            break; 
            
        case USB_DEVICE_EVENT_CONTROL_TRANSFER_DATA_RECEIVED:
            // This event indicates that a Control transfer Data has been received from Host.
            break; 
            
        case USB_DEVICE_EVENT_CONTROL_TRANSFER_ABORTED:
            // This event indicates a control transfer was aborted. 
            break; 
            
        case USB_DEVICE_EVENT_SUSPENDED:
            break;
            
        case USB_DEVICE_EVENT_RESUMED:
            break;
            
        case USB_DEVICE_EVENT_ERROR:
            break;
            
        case USB_DEVICE_EVENT_RESET:
            break;
            
        case USB_DEVICE_EVENT_SOF:
            // This event indicates an SOF is detected on the bus. The  USB_DEVICE_SOF_EVENT_ENABLE
            // macro should be defined to get this event. 
            break;
        default:
            break;
    }
}

void USBCDC_Init(CDCDevice* d){
    d->hDevice=USB_DEVICE_Open( d->deviceIndex, DRV_IO_INTENT_READWRITE );
    int size=USB_DEVICE_CDC_ReadPacketSizeGet(d->deviceIndex);
    d->RXBufHead=0;
    d->RXBufTail=0;
    d->TXBufPos=0;
    d->isConnected=false;
    USB_DEVICE_EventHandlerSet(d->hDevice, APP_USBDeviceEventHandler, (uintptr_t)d);
    
}