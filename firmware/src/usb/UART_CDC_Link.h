#ifndef UART_CDC_LINK_H
#define UART_CDC_LINK_H
#include "../cardReaderEmulator/src/3party/circular_buffer.h"
typedef enum{
 UART_CDC_LINK_CHECK_TX,
 UART_CDC_LINK_CHECK_RX,
 UART_CDC_SCHEDULE_SEND,
 UART_CDC_SCHEDULE_RECIVE
} UART_CDC_TASKS;

typedef struct{
    circBuf_t TX_FIFO_buffer;// { TX_FIFO_buffer_b,0,0,UART_FIFO_SIZE };
    circBuf_t RX_FIFO_buffer;// { TX_FIFO_buffer_b,0,0,UART_FIFO_SIZE };
    uint8_t deviceIndex;
    CDCDevice cdcDevice;
    UART_CDC_TASKS state;
    bool CDCsendPending;
    bool CDCRecivePending;
} UART_CDC;


void UART_CDC_Link_Tasks(UART_CDC* uartCDC);

#endif