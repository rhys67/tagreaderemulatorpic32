/*******************************************************************************
  MPLAB Harmony Application Header File

  Company:
    Microchip Technology Inc.

  File Name:
    app.h

  Summary:
    This header file provides prototypes and definitions for the application.

  Description:
    This header file provides function prototypes and data type definitions for
    the application.  Some of these are required by the system (such as the
    "APP_Initialize" and "APP_Tasks" prototypes) and some of them are only used
    internally by the application (such as the "APP_STATES" definition).  Both
    are defined here for convenience.
 *******************************************************************************/

#ifndef _APP_H
#define _APP_H

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system_definitions.h"
#include "cardReaderEmulator/src/reader_config.h"

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

extern "C" {

#endif
    // DOM-IGNORE-END 

    // *****************************************************************************
    // *****************************************************************************
    // Section: Type Definitions
    // *****************************************************************************
    // *****************************************************************************

    // *****************************************************************************
    /* Application states

      Summary:
        Application states enumeration

      Description:
        This enumeration defines the valid application states.  These states
        determine the behavior of the application at various times.
     */
#define INVALID_REG_ADDRESS 0xFF
#define EEPROM_I2C_ADDRESS 0x57<<1
//#define FIRMWARE_VERSION "1.1.0"
typedef enum {
        /* Application's state machine's initial state. */
        APP_STATE_INIT = 0,
        APP_STATE_SERVICE_TASKS,
        APP_STATE_IDLE,
        APP_STATE_START_TIMER,
        APP_STATE_CHECK_UART,
        APP_STATE_CHECK_CMD_CDC,
        EEPROM_READ_COMPLETE,
        APP_STATE_SOFT_RESET
        /* TODO: Define states used by the application state machine. */

    } APP_STATES;

    typedef enum {
        I2C_REQUEST_TYPE_READ,
        I2C_REQUEST_TYPE_WRITE,
        I2C_REQUEST_TYPE_NONE
    } I2C_REQUEST_TYPE;

    typedef struct {
        I2C_REQUEST_TYPE lastI2cOperation;
        char regAddress;
        bool gotAddress;
    } I2C_REQUEST;

    // *****************************************************************************
    /* Application Data

      Summary:
        Holds application data

      Description:
        This structure holds the application's data.

      Remarks:
        Application strings and buffers are be defined outside this structure.
     */
#include "cardReaderEmulator/src/uart_cmds.h"
#include "usb/USBCDC_wrapper.h"
#define I2C_READ_BUFFER_SIZE 2
#define I2C_WRITE_BUFFER_SIZE 4

    typedef struct {
        /* The application's current state */
        APP_STATES state;
        uint8_t i2cReadBuffer[I2C_READ_BUFFER_SIZE];
        DRV_I2C_BUFFER_HANDLE hI2cReadBuffer;
        uint8_t i2cWriteBuffer[I2C_WRITE_BUFFER_SIZE];
        DRV_I2C_BUFFER_HANDLE hI2cWriteBuffer;
        DRV_HANDLE hI2cSlave;
        DRV_HANDLE hReaderTimer;
        DRV_HANDLE hUptimeTimer;
        DRV_HANDLE hEEPROMI2c;
        uint8_t EEPROMI2cRequestBuffer[5];
        char EEPROMI2cResponseBuffer[16];
        I2C_REQUEST_TYPE lastEEpromRequest;
        DRV_I2C_BUFFER_HANDLE hTagDataEEPROMI2c;
        int8_t EEPROM_lastTagPageRequested;
        
        DRV_I2C_BUFFER_HANDLE hEEPROMConfigBuffer;
        DRV_I2C_BUFFER_HANDLE hEEPROMConfigSetBuffer;
        uint8_t EEPROMI2cConfigBuffer[1];
        GetConfigCompleted EEPROMI2cConfigCallback;
        SetConfigCompleted EEPROMI2cConfigSavedCallback;
        
        uint32_t upTimeCounter; 
        I2C_REQUEST currentRequest;
        UartCmdProcesser uartCmdHandler;
        CDCDevice readerCommandCDC;
        UartCmdProcesser readerCommandCDCHandler;
        bool readPending;
    } APP_DATA;
    /*
      Function:
        void APP_Initialize ( void )

      Summary:
         MPLAB Harmony application initialization routine.

      Description:
        This function initializes the Harmony application.  It places the 
        application in its initial state and prepares it to run so that its 
        APP_Tasks function can be called.

      Precondition:
        All other system initialization routines should be called before calling
        this routine (in "SYS_Initialize").

      Parameters:
        None.

      Returns:
        None.

      Example:
        <code>
        APP_Initialize();
        </code>

      Remarks:
        This routine must be called from the SYS_Initialize function.
     */

    void APP_Initialize(void);

    void APP_Tasks(void);
    
    
    bool APP_processGetRegRequest(char regAddress, unsigned char* data);
    bool APP_processSetRegRequest(char regAddress, unsigned char* data);


#endif /* _APP_H */

    //DOM-IGNORE-BEGIN
#ifdef __cplusplus
}
#endif
//DOM-IGNORE-END

/*******************************************************************************
 End of File
 */

