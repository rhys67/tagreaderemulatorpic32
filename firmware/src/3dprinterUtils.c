/**
    PN512 and NTAG213 Emulator 
    Copyright (C)  Rhys Bryant

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "cardReaderEmulator/src/tag/tag_ntag213.h"
#include "cardReaderEmulator/src/tag/tag_eeprom.h"
#include "3party/NfcKeyC.h"

#include "string.h"
#include "stdint.h"
#define UID_AUTO_GEN_MODE "AUTO"
#define FILAMENT_REMAINING_PAGE_INDEX 20
#define FILAMENT_REMAINING_CHECKSUM_MASK 0x54321248
#define FILAMENT_REMAINING_CHECKSUM_PAGE_INDEX 21
#define FILAMENT_TOTAL_PAGE_1_INDEX 10
#define FILAMENT_TOTAL_PAGE_2_INDEX 11

int nextEEPOMPageToWrite=0;
int remaing=0;

int bytesToInt(uint8_t* buffer) {
    return (buffer[0] ) | (buffer[1] << 8) | (buffer[2] << 16) | (buffer[3]<<24);
}

long bytesToLong(uint8_t* page1,uint8_t* page2) {
		long out=0;
		int i=0;
        uint8_t b=0;
		for( b=0;b<4 *8;b+=8){
            if( b < 32){
                out|=(page1[i++]<<b);
            }else{
                out|=(((long)page2[i++])<<b);
            }
		}
		return out;
}

void intToBytes(int val, uint8_t* buffer) {

    buffer[0] = (val & 0xFF);
    buffer[1] = ((val >> 8) & 0xff);
    buffer[2] = ((val >> 16) & 0xff);
    buffer[3] = ((val >> 24) & 0xff);
}

int getFilamentUsage() {
    return bytesToInt(Ntag_pages[FILAMENT_REMAINING_PAGE_INDEX]);
}

void setFilamentUsage(int val) {
    intToBytes(val, Ntag_pages[FILAMENT_REMAINING_PAGE_INDEX]);

    int cs = val ^ FILAMENT_REMAINING_CHECKSUM_MASK;
    intToBytes(cs, Ntag_pages[FILAMENT_REMAINING_CHECKSUM_PAGE_INDEX]);

}

long getTotalFilament(){
    return bytesToLong(Ntag_pages[FILAMENT_TOTAL_PAGE_1_INDEX],Ntag_pages[FILAMENT_TOTAL_PAGE_2_INDEX]);
}

/**
 * short of having a random number generator do some stuff based off the current uid
 * @param data
 * @param len
 */
void tranformData(uint8_t* data, int len) {
    int i = 0;
    for (i = 0; i < len; i++) {
        data[i] = (data[i]^(data[len-i]+1));
        //^Ntag_pages[data[i] & 0x44][data[i] & 0x3];
    }
}

void genNewUID(uint8_t* uid) {
    memcpy(uid, Ntag_pages[NTAG_UID_PAGE_INDEX_1], 3);
    memcpy(uid + 3, Ntag_pages[NTAG_UID_PAGE_INDEX_2], NTAG_NUM_BYTES_PER_PAGE);
    tranformData(uid, 4);
}

static void eeprom_write_finished(int8_t result,void* userData){
    
    switch(nextEEPOMPageToWrite){
        case NTAG_UID_PAGE_INDEX_1:
            nextEEPOMPageToWrite=NTAG_UID_PAGE_INDEX_2;
            break;
        case NTAG_UID_PAGE_INDEX_2:
            nextEEPOMPageToWrite=NTAG_AUTH_PACK_PAGE_INDEX;
            break;
        case NTAG_AUTH_PACK_PAGE_INDEX:
            nextEEPOMPageToWrite=FILAMENT_REMAINING_PAGE_INDEX;
            break;
        case FILAMENT_REMAINING_PAGE_INDEX:
            nextEEPOMPageToWrite=FILAMENT_REMAINING_CHECKSUM_PAGE_INDEX;
            break;
        case FILAMENT_REMAINING_CHECKSUM_PAGE_INDEX: 
        default:
            nextEEPOMPageToWrite=-1;
            tag_runtime_data.currentOperationCompletedCallback=NULL;
            return;
    }
    
    
    tag_eeprom_write_page(nextEEPOMPageToWrite, Ntag_pages[nextEEPOMPageToWrite], NTAG_NUM_BYTES_PER_PAGE);
}

void threedprinterUitls_updateTagUID() {

    int remaing= getFilamentUsage();
    if( remaing >= 1000 ){
        //more then 1 meter remaining no update required
        return;
    }
    
    uint8_t uid[7];
    genNewUID(uid);
    
    
    tag_ntag213_write_page(NTAG_UID_PAGE_INDEX_1, uid, 3);
    tag_ntag213_write_page(NTAG_UID_PAGE_INDEX_2, uid + 3, 4);
    
    uint16_t pack = getpack(uid);
    uint8_t packBytes[2] = "";
    packBytes[0] = pack & 0xFF;
    packBytes[1] = (pack >> 8) & 0xFF;
    tag_ntag213_write_page(NTAG_AUTH_PACK_PAGE_INDEX, packBytes, 2);
    
    long total=getTotalFilament();
    setFilamentUsage(total);

    //start the eeprom writing the next page updates are chained on the eeprom_write_finished callback
    nextEEPOMPageToWrite=NTAG_UID_PAGE_INDEX_1;
    tag_runtime_data.currentAsyncOperationCompletedCallbackUserData=&remaing;
    tag_runtime_data.currentOperationCompletedCallback=eeprom_write_finished;
    tag_eeprom_write_page(nextEEPOMPageToWrite, Ntag_pages[nextEEPOMPageToWrite], NTAG_NUM_BYTES_PER_PAGE);
    




}